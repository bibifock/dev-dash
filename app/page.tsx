"use client";
import { redirect } from "next/navigation";
import { useSession, signIn, signOut } from "next-auth/react";
import { Home } from "@/components/home";
import type { User } from "@/components/types";

export default function Page() {
  const { data: session, status } = useSession();
  const userEmail = session?.user?.email;

  if (status === "loading") {
    return <p>Hang on there...</p>;
  }

  if (status === "authenticated") {
    return <Home user={session.user as User} />;
  }

  signIn();

  return <p>Not signed in.</p>;
}
