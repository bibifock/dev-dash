import { gitlabDomain, nextAuthSecret } from "@/app/src/lib/config";
import { NextApiRequest } from "next";
import { getServerSession } from "next-auth";
import { getToken } from "next-auth/jwt";
import { getSession } from "next-auth/react";
import { NextResponse } from "next/server";
import { authOptions } from "../auth/[...nextauth]/route";

export async function GET(req: NextApiRequest) {
  const { accessToken: token } = await getToken({
    req,
  });

  const result = await fetch(`${gitlabDomain}/api/graphql`, {
    method: "POST",
    headers: {
      "content-type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      query: `
query projects {
  currentUser {
    projectMemberships(first: 50) {
      nodes {
        project {
          name
          webUrl
          avatarUrl
          mergeRequests(first: 50) {
            nodes {
              iid
              webUrl
              reviewers {
                nodes {
                  avatarUrl
                  username
                  webUrl
                }
              }
              sourceBranch
              targetBranch
              detailedMergeStatus
              descriptionHtml
              squashOnMerge
              titleHtml
           #   approved
              approvedBy {
                nodes {
                  avatarUrl
                  username
                  webUrl
                }
              }
              draft
              headPipeline {
                status
              }
              shouldBeRebased
              rebaseInProgress
              labels {
                nodes {
                  color
                  textColor
                  title
                }
              }
             # discussions {
             #   nodes {
             #     resolved
             #     notes {
             #       nodes {
             #         author {
             #           avatarUrl
             #           username
             #           webUrl
             #         }
             #         bodyHtml
             #       }
             #     }
             #   }
             # }
            }
          }
        }
      }
    }
  }
}
  `,
    }),
  });
  // return NextResponse.json({ test: true });

  // const result = await fetch(`${gitlabDomain}/api/v4/user`, {
  // headers: {
  // Authorization: `Bearer ${token}`,
  // },
  // });
  const json = await result.json();
  // console.log(json);

  return NextResponse.json(json);
}
