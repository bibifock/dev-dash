import NextAuth from "next-auth";
import GitlabProvider from "next-auth/providers/gitlab";

import {
  gitlabClientId,
  gitlabClientSecret,
  gitlabDomain,
  gitlabProvider,
} from "@/lib/config";
import type { User } from "@/components/types";

type GitlabProfile = {
  name: string;
  nickname?: string;
  email: string;
  preferred_username: string;
  profile: string;
  picture: string;
};

const gitlabMattersProvider = {
  ...GitlabProvider({
    clientId: gitlabClientId,
    clientSecret: gitlabClientSecret,
  }),
  idToken: true,
  jwt: true,
  id: gitlabProvider,
  name: gitlabProvider,
  wellKnown: gitlabDomain,
  profile(profile: GitlabProfile): User {
    return {
      id: profile.email,
      name: profile.name,
      username: profile.nickname ?? profile.preferred_username,
      email: profile.email,
      profile: profile.profile,
      image: profile.picture,
    };
  },
  authorization: {
    url: `${gitlabDomain}/oauth/authorize`,
    params: {
      scope: [
        "read_api",
        "read_user",
        "read_repository",
        "read_registry",
        "read_observability",
        "openid",
        "profile",
        "email",
      ].join(" "),
    },
  },
  token: `${gitlabDomain}/oauth/token`,
  userinfo: `${gitlabDomain}/api/v4/user`,
};

export const authOptions = {
  debug: false,
  providers: [gitlabMattersProvider],
  callbacks: {
    async jwt({
      token,
      account,
    }: {
      token: { accessToken?: string; refreshToken?: string };
      account: { access_token?: string; refresh_token?: string };
    }) {
      return {
        ...token,
        accessToken: token?.accessToken ?? account?.access_token,
        refreshToken: token?.refreshToken ?? account?.refresh_token,
      };
    },
  },
};

const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };
