export const gitlabDomain = process.env.GITLAB_URL ?? "https://gitlab.com";
export const gitlabProvider = process.env.GITLAB_PROVIDER_NAME ?? "gitlab";
export const gitlabClientId = process.env.GITLAB_APPLICATION_ID ?? "";
export const gitlabClientSecret = process.env.GITLAB_APPLICATION_SECRET ?? "";
export const nextAuthSecret = process.env.NEXT_AUTH_SECRET ?? "";
