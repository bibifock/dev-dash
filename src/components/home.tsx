import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Settings } from "@/components/settings";
import { User } from "@/components/types";
import { Button } from "./ui/button";
import { LogOut } from "lucide-react";
import { Suspense } from "react";
import { signOut } from "next-auth/react";

export const Home = ({ user }: { user: User }) => {
  return (
    <div>
      <div>
        {/*
        <Avatar>
          <AvatarImage src={user.image} alt="user-image" />
          <AvatarFallback>
            {(user.name.match(/\b\p{L}/gu) ?? []).splice(0, 2).join("")}
          </AvatarFallback>
        </Avatar>
          */}
        <Button variant="outline" size="icon" onClick={() => signOut()}>
          <LogOut className="w-6 h-6" />
        </Button>
      </div>
      <Suspense fallback={<div>loading...</div>}>
        <Settings />
      </Suspense>
    </div>
  );
};
