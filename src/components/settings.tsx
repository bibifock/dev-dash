import { nextAuthSecret, gitlabDomain } from "@/lib/config";
import { getServerSession } from "next-auth";
import { getToken } from "next-auth/jwt";
import { getSession, useSession } from "next-auth/react";
import { useEffect, useState } from "react";

// async function getProjects() {
// const res = await fetch("/api/projects");
// return res.json();
// }

export const Settings = async () => {
  const [projects, setProjects] = useState();

  useEffect(() => {
    fetch("/api/projects")
      .then((r) => r.json())
      .then(setProjects);
  }, []);

  return (
    <>
      <div>settings</div>
      <pre>{JSON.stringify(projects)}</pre>
    </>
  );
};
